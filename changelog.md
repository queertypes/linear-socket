# 0.3.3.3 (Sep 29, 2018)

* fix some build issues
* update copyright year

# 0.3.3.0 (Dec. 3, 2017)

* remove ynot-prelude dependency

# 0.3.2.0 (Dec. 3, 2017)

* make CanShutdown constraints tighter

# 0.3.1.0 (Dec. 2, 2017)

* added socket creation functions that make things easier to use
  * tcp4Socket
  * tcp6Socket
  * tcpUnixSocket
  * and UDP versions

# 0.3.0.0 (Dec. 2, 2017)

* made API friendlier for point-free operations
* update type-level structures to make better use of shutdown status tracking

# 0.2.0.0 (Dec. 2, 2017)

* released

# 0.1.0.0 (November 29, 2017)

* init
